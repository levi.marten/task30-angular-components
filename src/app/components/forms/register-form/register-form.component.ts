import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  @Input() formTitle: string;

  @Output() signUpAttempt: EventEmitter<any> = new EventEmitter;

  private signUpResult: any = {
    isSignedUp: false,
    message: ''
  };
  
  private counter: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  onSignUpClicked() {

    this.counter++

    if (this.counter % 2 == 0) {
      this.signUpResult.isSignedUp = true;
      this.signUpResult.message = 'Yay, you are now signed up!'
    } else {
      this.signUpResult.isSignedUp = false;
      this.signUpResult.message = 'You could not sign up right now...'
    }
    this.signUpAttempt.emit(this.signUpResult)
  }
}
