import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'task30-angular-components';

  onLoginAttempted(loginResult: any): void {
    if (loginResult.isLoggedIn) {
      // Redirect
    }
    alert(loginResult.message)
  }

  onSignUpAttempted(signUpResult: any): void {
    if (signUpResult.isSignedUp) {
      // Redirect
    }
    alert(signUpResult.message)
  }
};


