# Task30AngularComponents

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.

## Task description
•Create a Login Form component
•Create a Register Form component

Both should have the following inputs:
- Username
- Password
•The Register form should ALSO have Confirm Password
•Both components should have a button that emits a string to it’s parent.
•Both components should have an input that receives a string from it’s parent

Try to add the Login form and Register form component to the App Component. Check that it registers and that you can see the message received from the App Component and receive a message from the Child component (Login and Register).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
